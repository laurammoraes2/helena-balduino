const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const app = express();
const exphbs  = require('express-handlebars');
// const Usuario = require('./models/Cadastro.js')
//
// // body-parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));
//
//
// // config
//     // Template engine
app.engine('handlebars', exphbs({defaultLayout: 'main'}))
app.set('view engine', 'handlebars')


//Rotas
app.get('/', function(req,res){
  res.render('index')
})
// app.get('/quemsou', function(req,res){
//   res.render('quemsou')
// })
app.get('/agenda', function(req,res){
  res.render('agenda')
})
app.get('/contato', function(req,res){
  res.render('contato')
})







app.listen(3000, function(){
  console.log("Listening on port 3000!")
});
